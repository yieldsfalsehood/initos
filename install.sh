#!/bin/sh

set -eu

INSTALL_DIR="$1"

INITOS_EXECVM_VERSION=$(jq -r .execvm_version version.json)
INITOS_SCALL_VERSION=$(jq -r .scall_version version.json)

EXECVM="https://gitlab.com/api/v4/projects/23209558"
SCALL="https://gitlab.com/yields.falsehood/scall/-/releases"

chroot "$INSTALL_DIR" \
       /bin/tar -xzf - -C / < rootfs.tar.gz

wget -O scall.tar.gz \
     "${SCALL}/${INITOS_SCALL_VERSION}/downloads/scall.tar.gz"

chroot "$INSTALL_DIR" \
       /bin/tar -xzf - -C / < scall.tar.gz

# i don't like this but i'm not ready to fix it yet. i don't really
# need or want networking in initos itself. i don't even install a
# package manager because it's not that much of an "os". running pip
# in the chroot requires networking though which is a little
# convenient for us here, but i think that's coming at a high
# technical debt. i think eventually once i settle on the "base os"
# (probably alpine, realistically), i'll produce os packages for scall
# and execvm. that way i can orchestrate dependency management at the
# host level, the guest (initos) never reaches the internet, and this
# script reduces to something like "apk add scall execvm".
cp /etc/resolv.conf /mnt/etc/resolv.conf

chroot "$INSTALL_DIR" \
       /usr/bin/pip \
           install execvm=="${INITOS_EXECVM_VERSION}" \
           --extra-index-url "${EXECVM}/packages/pypi/simple"

rm /mnt/etc/resolv.conf
