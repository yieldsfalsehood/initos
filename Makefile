
# https://stackoverflow.com/a/7367903
GUARD-%:
	@if [ "${${*}}" = "" ]; then \
	    echo "Environment variable $* not set"; \
	    exit 1; \
	fi

rootfs.tar.gz: rootfs.mtree
	bsdtar -czf rootfs.tar.gz @rootfs.mtree

.PHONY: install-alpine
install-alpine: GUARD-INSTALL_DIR
	sh install-alpine.sh $(INSTALL_DIR)

.PHONY: install
install: GUARD-INSTALL_DIR
	sh install.sh $(INSTALL_DIR)

initos.tar.gz: GUARD-INSTALL_DIR
	tar -czf $@ -C $(INSTALL_DIR) .

initos.squashfs: GUARD-INSTALL_DIR
	mksquashfs $(INSTALL_DIR) $@ -no-xattrs
