#!/bin/sh

set -eu

INSTALL_DIR="$1"

apk add \
    --repositories-file /etc/apk/repositories \
    --keys-dir /etc/apk/keys \
    --root "$INSTALL_DIR" \
    --initdb \
    --no-cache \
    alpine-base \
    python3 \
    py3-pip
